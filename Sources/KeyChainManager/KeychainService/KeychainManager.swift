
import Foundation

public final class KeychainManager {
    static var bundleName: String {
        return Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? UUID().uuidString
    }

    public init() { }

    private var keychain = KeychainSwift(keyPrefix: KeychainManager.bundleName)

    public func set(_ data: String, forKey name: String) {
        keychain.set(data, forKey: name)
    }

    public func get(forKey name: String) -> String? {
        return keychain.get(name)
    }

    public func delete(forKey name: String) {
        keychain.delete(name)
    }

    public func cleanKeychain() {
        keychain.clear()
    }
}
