# KeyChainManager

Для сохранения вызвать метод:

KeychainManager().set("Значение", forKey: "Ключ")

Для чтения:

KeychainManager().get(forKey: "Ключ")

Для удаления:

KeychainManager().delete(forKey: "Ключ")

Для полной очистки:

KeychainManager().cleanKeychain()
